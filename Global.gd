# configured as autoload
extends Node

@onready var island_window_pack = preload("res://island_window.tscn")
@onready var world: Node2D = $/root/World

func quit():
	get_tree().quit()

func instantiate_island():
	world.add_child(island_window_pack.instantiate())
