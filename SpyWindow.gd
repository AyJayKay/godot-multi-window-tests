extends Window

@onready var camera: Camera2D = $Camera2D
@onready var world_offset_on_screen = DisplayServer.get_display_safe_area().position

func _process(delta):
	camera.position = position - world_offset_on_screen
