extends Node2D

var sub_window_script = load("res://IslandWindow.gd")
var main_window: Window
@onready var spy_window: Window = $SpyWindow

func _ready():
	# copy island window behaviour to main window (not accesable by editor hierarchy)
	main_window = get_tree().get_root()
	main_window.script = sub_window_script
	spy_window.world_2d = main_window.world_2d
	
	# position main window
	'var screen_id = DisplayServer.window_get_current_screen(main_window.get_window_id())
	var screen_position = DisplayServer.screen_get_position(screen_id)
	var screen_size = DisplayServer.screen_get_size(screen_id)
	main_window.position = Vector2i(main_window.position.x, screen_size.y - main_window.size.y + screen_position.y)'
	var screen_area = DisplayServer.get_display_safe_area() # only for active screen
	main_window.position = Vector2i(main_window.position.x, screen_area.size.y - main_window.size.y + screen_area.position.y)
