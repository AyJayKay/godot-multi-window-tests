extends Node

# duplicate functionality from IslandWindow.gd to test mixin approach by sub-nodes
func _input(event):
	# instantiating generic island window
	if event is InputEventKey and event.keycode == KEY_SPACE and event.pressed:
		Global.instantiate_island()
