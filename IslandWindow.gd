# used also on the main window
extends Window

var dragging = false
var offset = Vector2i()

func _input(event):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT:
		if event.pressed:
			var window_rect = Rect2i(get_position(), Vector2i(128, 128))
			if window_rect.has_point(get_screen_mouse_position()):
				dragging = true
				offset = get_position() - get_screen_mouse_position()
		if event.is_released() or event.is_canceled():
			dragging = false
	
	if event is InputEventKey and event.keycode == KEY_ESCAPE and event.pressed:
		Global.quit()
	
	# instantiating generic island window
	if event is InputEventKey and event.keycode == KEY_SPACE and event.pressed:
		Global.instantiate_island()

func _process(delta):
	if dragging:
		set_position(get_screen_mouse_position() + offset)
	var window_rect = Rect2i(get_position(), Vector2i(128, 128))
	'if window_rect.has_point(get_screen_mouse_position()):
		move_to_foreground()'

func _notification(what):
	match what:
		MainLoop.NOTIFICATION_APPLICATION_FOCUS_IN:
			print(name)

func get_screen_mouse_position():
	return Vector2i(get_mouse_position().x + get_position().x, get_mouse_position().y + get_position().y)
